// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/events"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

func handlerListNonce(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	nonceRepository, _ := context.Value(nonceRepositoryKey).(repositories.NonceRepository)

	event := events.SES_12
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	nonces := nonceRepository.ListNonce(context)

	responseWriter.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(responseWriter).Encode(nonces)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_13
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerCreateNonce(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	nonceRepository, _ := context.Value(nonceRepositoryKey).(repositories.NonceRepository)

	event := events.SES_14
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var nonce model.Nonce
	err := json.NewDecoder(request.Body).Decode(&nonce)
	if err != nil {
		event := events.SES_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	createdNonce, err := nonceRepository.CreateNonce(context, nonce)
	if err != nil {
		event := events.SES_24
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		logger.Error("error retrieving nonce: %v", zap.Error(err))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*createdNonce)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_15
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerGetNonce(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	nonceRepository, _ := context.Value(nonceRepositoryKey).(repositories.NonceRepository)

	event := events.SES_16
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	nonce, err := nonceRepository.GetNonce(context, chi.URLParam(request, "nonce"))
	if errors.Is(err, repositories.ErrNonceNotFound) {
		event := events.SES_25
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}
	if err != nil {
		event := events.SES_24
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*nonce)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_17
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerUpdateNonce(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	nonceRepository, _ := context.Value(nonceRepositoryKey).(repositories.NonceRepository)

	event := events.SES_18
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var nonce model.Nonce
	err := json.NewDecoder(request.Body).Decode(&nonce)
	if err != nil {
		event := events.SES_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	nonceString := chi.URLParam(request, "nonce")

	_, err = nonceRepository.GetNonce(context, nonceString)
	if err != nil {
		event := events.SES_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		logger.Error("failed to get nonce setting")
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	_, err = nonceRepository.UpdateNonce(context, nonce, nonceString)
	if err != nil {
		event := events.SES_27
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		logger.Error("error updating nonce: %v", zap.Error(err))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(nonce)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_19
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerDeleteNonce(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	nonceRepository, _ := context.Value(nonceRepositoryKey).(repositories.NonceRepository)

	event := events.SES_20
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	nonceString := chi.URLParam(request, "nonce")

	nonce, err := nonceRepository.GetNonce(context, nonceString)
	if err != nil {
		event := events.SES_26
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = nonceRepository.DeleteNonce(context, nonceString)
	if err != nil {
		event := events.SES_28
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		logger.Error("error deleting nonce: %v", zap.Error(err))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*nonce)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_21
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
