// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"context"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/internal/http_infra/metrics"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/internal/repositories"
)

type key int

const (
	nonceRepositoryKey   key = iota
	sessionRepositoryKey key = iota
	loggerKey            key = iota
)

func NewRouter(nonceRepository repositories.NonceRepository, sessionRepository repositories.SessionRepository, apiKey string, logger *zap.Logger) *chi.Mux {
	r := chi.NewRouter()

	cors := cors.New(cors.Options{
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
	})
	r.Use(cors.Handler)

	metricsMiddleware := metrics.NewMiddleware("session-service")
	r.Use(metricsMiddleware)
	r.Handle("/metrics", promhttp.Handler())

	r.Route("/v1", func(r chi.Router) {
		r.Use(middleware.SetHeader("API-Version", "1.0.0"))

		r.Route("/nonces", func(r chi.Router) {
			r.Use(middleware.Logger)

			r.Use(authenticatedOnly(apiKey, logger))

			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), nonceRepositoryKey, nonceRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerListNonce(responseWriter, request.WithContext(ctx))
			})
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), nonceRepositoryKey, nonceRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerCreateNonce(responseWriter, request.WithContext(ctx))
			})
			r.Get("/{nonce}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), nonceRepositoryKey, nonceRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerGetNonce(responseWriter, request.WithContext(ctx))
			})
			r.Put("/{nonce}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), nonceRepositoryKey, nonceRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerUpdateNonce(responseWriter, request.WithContext(ctx))
			})
			r.Delete("/{nonce}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), nonceRepositoryKey, nonceRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerDeleteNonce(responseWriter, request.WithContext(ctx))
			})
		})

		r.Route("/sessions", func(r chi.Router) {
			r.Use(middleware.Logger)

			r.Use(authenticatedOnly(apiKey, logger))

			r.Get("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), sessionRepositoryKey, sessionRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerListSession(responseWriter, request.WithContext(ctx))
			})
			r.Post("/", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), sessionRepositoryKey, sessionRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerCreateSession(responseWriter, request.WithContext(ctx))
			})
			r.Get("/{sessionToken}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), sessionRepositoryKey, sessionRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerGetSession(responseWriter, request.WithContext(ctx))
			})
			r.Put("/{sessionToken}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), sessionRepositoryKey, sessionRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerUpdateSession(responseWriter, request.WithContext(ctx))
			})
			r.Delete("/{sessionToken}", func(responseWriter http.ResponseWriter, request *http.Request) {
				ctx := context.WithValue(request.Context(), sessionRepositoryKey, sessionRepository)
				ctx = context.WithValue(ctx, loggerKey, logger)
				handlerDeleteSession(responseWriter, request.WithContext(ctx))
			})
		})

		r.Get("/openapi.json", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerJson(responseWriter, request.WithContext(ctx))
		})

		r.Get("/openapi.yaml", func(responseWriter http.ResponseWriter, request *http.Request) {
			ctx := context.WithValue(request.Context(), loggerKey, logger)
			handlerYaml(responseWriter, request.WithContext(ctx))
		})

		healthCheckHandler := healthcheck.NewHandler("session-service", []healthcheck.Checker{nonceRepository})
		r.Route("/health", func(r chi.Router) {
			r.Get("/", healthCheckHandler.HandleHealth)
			r.Get("/check", healthCheckHandler.HandlerHealthCheck)
		})
	})

	return r
}

func authenticatedOnly(apiKey string, logger *zap.Logger) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(responseWriter http.ResponseWriter, request *http.Request) {
			providedApiKey := request.Header.Get("Authentication")

			if providedApiKey != apiKey {
				logger.Error("Unauthorized: apikey invalid")
				http.Error(responseWriter, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}

			next.ServeHTTP(responseWriter, request)
		})
	}
}
