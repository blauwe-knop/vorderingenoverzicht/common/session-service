// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package metrics

import (
	"net/http"
	"strconv"
	"time"

	middlewarePkg "github.com/go-chi/chi/v5/middleware"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	dflBuckets = []float64{300, 1200, 5000}
)

const (
	requestsName = "chi_requests_total"
	latencyName  = "chi_request_duration_milliseconds"
)

// Middleware is a handler that exposes prometheus metrics for app info, the number of requests,
// the latency and the response size, partitioned by status code, method and HTTP path.
// This implementation is based on https://github.com/766b/chi-prometheus
type Middleware struct {
	appInfo  prometheus.Gauge
	requests *prometheus.CounterVec
	latency  *prometheus.HistogramVec
}

// NewMiddleware returns a new prometheus Middleware handler.
func NewMiddleware(applicationName string, buckets ...float64) func(nextHandler http.Handler) http.Handler {
	var middleware Middleware

	middleware.appInfo = promauto.NewGauge(
		prometheus.GaugeOpts{
			Name:        "appInfo",
			Help:        "Info about the application",
			ConstLabels: prometheus.Labels{"app": applicationName},
		})
	middleware.appInfo.Set(1)

	middleware.requests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name:        requestsName,
			Help:        "How many HTTP requests processed, partitioned by status code, method and HTTP path.",
			ConstLabels: prometheus.Labels{"app": applicationName},
		},
		[]string{"code", "method", "host", "path"},
	)
	prometheus.MustRegister(middleware.requests)

	if len(buckets) == 0 {
		buckets = dflBuckets
	}
	middleware.latency = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Name:        latencyName,
		Help:        "How long it took to process the request, partitioned by status code, method and HTTP path.",
		ConstLabels: prometheus.Labels{"app": applicationName},
		Buckets:     buckets,
	},
		[]string{"code", "method", "host", "path"},
	)
	prometheus.MustRegister(middleware.latency)

	return middleware.handler
}

func (middleware Middleware) handler(nextHandler http.Handler) http.Handler {
	metricsHandlerFunction := func(response http.ResponseWriter, request *http.Request) {
		start := time.Now()
		wrapResponseWriter := middlewarePkg.NewWrapResponseWriter(response, request.ProtoMajor)
		nextHandler.ServeHTTP(wrapResponseWriter, request)
		middleware.requests.WithLabelValues(strconv.Itoa(wrapResponseWriter.Status()), request.Method, request.Host, request.URL.Path).Inc()
		middleware.latency.WithLabelValues(strconv.Itoa(wrapResponseWriter.Status()), request.Method, request.Host, request.URL.Path).Observe(float64(time.Since(start).Nanoseconds()) / 1000000)
	}
	return http.HandlerFunc(metricsHandlerFunction)
}
