// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package http_infra

import (
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-chi/chi/v5"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/internal/repositories"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/events"
	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

func handlerListSession(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	sessionRepository, _ := context.Value(sessionRepositoryKey).(repositories.SessionRepository)

	event := events.SES_29
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	sessions := sessionRepository.ListSession(context)

	responseWriter.Header().Set("Content-Type", "application/json")
	err := json.NewEncoder(responseWriter).Encode(sessions)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_30
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerCreateSession(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	sessionRepository, _ := context.Value(sessionRepositoryKey).(repositories.SessionRepository)

	event := events.SES_31
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var session model.Session
	err := json.NewDecoder(request.Body).Decode(&session)
	if err != nil {
		event := events.SES_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	createdSession, err := sessionRepository.CreateSession(context, session)
	if err != nil {
		event := events.SES_39
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		logger.Error("error retrieving session: %v", zap.Error(err))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*createdSession)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_32
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerGetSession(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	sessionRepository, _ := context.Value(sessionRepositoryKey).(repositories.SessionRepository)

	event := events.SES_33
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	session, err := sessionRepository.GetSession(context, chi.URLParam(request, "sessionToken"))
	if errors.Is(err, repositories.ErrSessionNotFound) {
		event := events.SES_40
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "Not Found", http.StatusNotFound)
		return
	}
	if err != nil {
		event := events.SES_41
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*session)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_34
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerUpdateSession(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	sessionRepository, _ := context.Value(sessionRepositoryKey).(repositories.SessionRepository)

	event := events.SES_35
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	var session model.Session
	err := json.NewDecoder(request.Body).Decode(&session)
	if err != nil {
		event := events.SES_23
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	sessionToken := chi.URLParam(request, "sessionToken")

	_, err = sessionRepository.GetSession(context, sessionToken)
	if err != nil {
		event := events.SES_41
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	_, err = sessionRepository.UpdateSession(context, session, sessionToken)
	if err != nil {
		event := events.SES_42
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(session)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_36
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}

func handlerDeleteSession(responseWriter http.ResponseWriter, request *http.Request) {
	context := request.Context()
	logger := context.Value(loggerKey).(*zap.Logger)
	sessionRepository, _ := context.Value(sessionRepositoryKey).(repositories.SessionRepository)

	event := events.SES_37
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))

	sessionToken := chi.URLParam(request, "sessionToken")

	session, err := sessionRepository.GetSession(context, sessionToken)
	if err != nil {
		event := events.SES_41
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	err = sessionRepository.DeleteSession(context, sessionToken)
	if err != nil {
		event := events.SES_43
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}

	responseWriter.Header().Set("Content-Type", "application/json")
	err = json.NewEncoder(responseWriter).Encode(*session)
	if err != nil {
		event := events.SES_22
		logger.Log(event.GetLogLevel(), event.Message, zap.Error(err), zap.Reflect("event", event))
		http.Error(responseWriter, "internal error", http.StatusInternalServerError)
		return
	}
	event = events.SES_38
	logger.Log(event.GetLogLevel(), event.Message, zap.Reflect("event", event))
}
