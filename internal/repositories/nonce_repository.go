// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

type NonceRepository interface {
	CreateNonce(ctx context.Context, newNonce model.Nonce) (*model.Nonce, error)
	ListNonce(ctx context.Context) []model.Nonce
	GetNonce(ctx context.Context, nonce string) (*model.Nonce, error)
	UpdateNonce(ctx context.Context, newNonce model.Nonce, nonce string) (*model.Nonce, error)
	DeleteNonce(ctx context.Context, nonce string) error
	healthcheck.Checker
}
