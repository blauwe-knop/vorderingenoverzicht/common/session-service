// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

type SessionRepository interface {
	CreateSession(ctx context.Context, newSession model.Session) (*model.Session, error)
	ListSession(ctx context.Context) []model.Session
	GetSession(ctx context.Context, sessionToken string) (*model.Session, error)
	UpdateSession(ctx context.Context, newSession model.Session, sessionToken string) (*model.Session, error)
	DeleteSession(ctx context.Context, sessionToken string) error
	healthcheck.Checker
}
