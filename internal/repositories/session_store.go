// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

type SessionStore struct {
	log *zap.Logger

	sessionList []model.Session
}

var ErrSessionNotFound = errors.New("session does not exist")

func NewSessionStore(ctx context.Context, log *zap.Logger) *SessionStore {
	return &SessionStore{log: log, sessionList: []model.Session{}}
}

func (s *SessionStore) CreateSession(ctx context.Context, newSession model.Session) (*model.Session, error) {
	s.log.Debug("Create", zap.String("session.token", newSession.Token))

	s.sessionList = append(s.sessionList, newSession)

	return &newSession, nil
}

func (s *SessionStore) ListSession(ctx context.Context) []model.Session {
	return s.sessionList
}

func (s *SessionStore) GetSession(ctx context.Context, sessionToken string) (*model.Session, error) {
	s.log.Debug("Get", zap.String("sessionToken", fmt.Sprint(sessionToken)))
	result := []model.Session{}

	for _, s := range s.sessionList {
		if s.Token == sessionToken {
			result = append(result, s)
			break
		}
	}

	if len(result) == 0 {
		return nil, ErrSessionNotFound
	}

	return &result[0], nil
}

func (s *SessionStore) UpdateSession(ctx context.Context, newSession model.Session, sessionToken string) (*model.Session, error) {
	s.log.Debug("Update", zap.String("sessionToken", fmt.Sprint(sessionToken)))

	found := false
	for i, n := range s.sessionList {
		if n.Token == sessionToken {
			s.sessionList[i].AppPublicKey = newSession.AppPublicKey
			s.sessionList[i].CreatedAt = newSession.CreatedAt
			s.sessionList[i].ExpiresAt = newSession.ExpiresAt
			s.sessionList[i].Token = newSession.Token
			s.sessionList[i].AesKey = newSession.AesKey
			s.sessionList[i].Scope = newSession.Scope
			s.sessionList[i].Bsn = newSession.Bsn
			found = true
			break
		}
	}

	if !found {
		return nil, ErrSessionNotFound
	}

	return &newSession, nil
}

func (s *SessionStore) DeleteSession(ctx context.Context, sessionToken string) error {
	s.log.Debug("Delete", zap.String("sessionToken", fmt.Sprint(sessionToken)))

	var foundIndex *int
	for i, n := range s.sessionList {
		if n.Token == sessionToken {
			foundIndex = &i
			break
		}
	}

	if foundIndex == nil {
		return ErrSessionNotFound
	}

	s.sessionList = append(s.sessionList[:*foundIndex], s.sessionList[*foundIndex+1:]...)

	return nil
}

func (s *SessionStore) GetHealthCheck() healthcheck.Result {
	return healthcheck.Result{
		Name:         "session-store",
		Status:       healthcheck.StatusOK,
		ResponseTime: 0,
		HealthChecks: []healthcheck.Result{},
	}
}
