// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"go.uber.org/zap"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

type NonceStore struct {
	log *zap.Logger

	nonceList []model.Nonce
}

var ErrNonceNotFound = errors.New("nonce does not exist")

func NewNonceStore(ctx context.Context, log *zap.Logger) *NonceStore {
	return &NonceStore{log: log, nonceList: []model.Nonce{}}
}

func (s *NonceStore) CreateNonce(ctx context.Context, newNonce model.Nonce) (*model.Nonce, error) {
	s.log.Debug("Create", zap.String("nonce.nonce", newNonce.Nonce))

	s.nonceList = append(s.nonceList, newNonce)

	return &newNonce, nil
}

func (s *NonceStore) ListNonce(ctx context.Context) []model.Nonce {
	return s.nonceList
}

func (s *NonceStore) GetNonce(ctx context.Context, nonce string) (*model.Nonce, error) {
	s.log.Debug("Get", zap.String("nonce", fmt.Sprint(nonce)))
	result := []model.Nonce{}

	for _, s := range s.nonceList {
		if s.Nonce == nonce {
			result = append(result, s)
			break
		}
	}

	if len(result) == 0 {
		return nil, ErrNonceNotFound
	}

	return &result[0], nil
}

func (s *NonceStore) UpdateNonce(ctx context.Context, newNonce model.Nonce, nonce string) (*model.Nonce, error) {
	s.log.Debug("Update", zap.String("nonce", fmt.Sprint(nonce)))

	found := false
	for i, n := range s.nonceList {
		if n.Nonce == nonce {
			s.nonceList[i].EphemeralOrganizationKeyPair = newNonce.EphemeralOrganizationKeyPair
			s.nonceList[i].CreatedAt = newNonce.CreatedAt
			s.nonceList[i].Nonce = newNonce.Nonce
			found = true
			break
		}
	}

	if !found {
		return nil, ErrNonceNotFound
	}

	return &newNonce, nil
}

func (s *NonceStore) DeleteNonce(ctx context.Context, nonce string) error {
	s.log.Debug("Delete", zap.String("nonce", fmt.Sprint(nonce)))

	var foundIndex *int
	for i, n := range s.nonceList {
		if n.Nonce == nonce {
			foundIndex = &i
			break
		}
	}

	if foundIndex == nil {
		return ErrNonceNotFound
	}

	s.nonceList = append(s.nonceList[:*foundIndex], s.nonceList[*foundIndex+1:]...)

	return nil
}

func (s *NonceStore) GetHealthCheck() healthcheck.Result {
	return healthcheck.Result{
		Name:         "nonce-store",
		Status:       healthcheck.StatusOK,
		ResponseTime: 0,
		HealthChecks: []healthcheck.Result{},
	}
}
