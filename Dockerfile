FROM golang:1.23.1-alpine AS build

RUN apk add --update --no-cache git

COPY ./api /go/src/session-service/api
COPY ./cmd /go/src/session-service/cmd
COPY ./internal /go/src/session-service/internal
COPY ./pkg /go/src/session-service/pkg
COPY ./go.mod /go/src/session-service/
COPY ./go.sum /go/src/session-service/
WORKDIR /go/src/session-service
RUN go mod download \
 && go build -o dist/bin/session-service ./cmd/session-service

# Release binary on latest alpine image.
FROM alpine:3.20

ARG USER_ID=10001
ARG GROUP_ID=10001

COPY --from=build /go/src/session-service/dist/bin/session-service /usr/local/bin/session-service
COPY --from=build /go/src/session-service/api/openapi.json /api/openapi.json
COPY --from=build /go/src/session-service/api/openapi.yaml /api/openapi.yaml

# Add non-priveleged user. Disabled for openshift
RUN addgroup -g "$GROUP_ID" appgroup \
 && adduser -D -u "$USER_ID" -G appgroup appuser
USER appuser
HEALTHCHECK none
CMD ["/usr/local/bin/session-service"]
