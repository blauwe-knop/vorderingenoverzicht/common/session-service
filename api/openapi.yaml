openapi: 3.0.1
info:
  title: Vorderingenoverzicht - Session service API
  description: Servicecomponent to make CRUD actions available for the session.
  version: 1.0.0
externalDocs:
  description: Gitlab repository
  url: https://gitlab.com/blauwe-knop/vorderingenoverzicht/session-service
servers:
  - url: http://session-service/v1
paths:
  /v1/nonces:
    get:
      summary: Get list of nonces.
      operationId: getNonces
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Nonce'
    post:
      summary: Create a nonce.
      operationId: createNonce
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Nonce'
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Nonce'
  /v1/nonces/{nonce}:
    get:
      summary: Find nonce by ID
      operationId: getNonceById
      parameters:
        - name: nonce
          in: path
          description: ID of nonce to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Nonce'
    put:
      summary: Edit nonce by ID
      operationId: EditNonceById
      parameters:
        - name: nonce
          in: path
          description: ID of nonce to update
          required: true
          schema:
            type: string
      requestBody:
        description: Edited nonce object
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Nonce'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Nonce'
    delete:
      summary: Delete nonce by ID
      operationId: deleteNonceById
      parameters:
        - name: nonce
          in: path
          description: ID of nonce to delete
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Nonce'
  /v1/sessions:
    get:
      summary: Get list of sessions.
      operationId: getSessions
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Session'
    post:
      summary: Create a session.
      operationId: createSession
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Session'
      responses:
        '200':
          description: Successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Session'
  /v1/sessions/{sessionToken}:
    get:
      summary: Find session by ID
      operationId: getSessionById
      parameters:
        - name: sessionToken
          in: path
          description: ID of session to return
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Session'
    put:
      summary: Edit session by ID
      operationId: EditSessionById
      parameters:
        - name: sessionToken
          in: path
          description: ID of session to update
          required: true
          schema:
            type: string
      requestBody:
        description: Edited session object
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Session'
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Session'
    delete:
      summary: Delete session by ID
      operationId: deleteSessionById
      parameters:
        - name: sessionToken
          in: path
          description: ID of session to delete
          required: true
          schema:
            type: string
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Session'
  /v1/health:
    get:
      summary: Check health of component.
      operationId: getHealth
      responses:
        '200':
          description: successful operation
  /v1/health/check:
    get:
      summary: Check health of component and its dependencies.
      operationId: getHealthCheck
      responses:
        '200':
          description: successful operation
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/HealthCheck"
components:
  schemas:
    Nonce:
      type: object
      properties:
        nonce:
          type: string
        appPublicKey:
          type: string
        createdAt:
          type: string
          format: date-time
    Session:
      type: object
      properties:
        token:
          type: string
        appPublicKey:
          type: string
        createdAt:
          type: string
          format: date-time
        expiresAt:
          type: string
          format: date-time
    HealthCheck:
      type: object
      properties:
        name:
          type: string
        status:
          type: string
        responseTime:
          type: number
        healthChecks:
          type: array
          items:
            $ref: '#/components/schemas/HealthCheck'
      example:
        name: component
        status: "OK"
        responseTime: 1.0
        healthChecks:
          name: child-component
          status: "OK"
          responseTime: 1.0
          healthChecks: []
