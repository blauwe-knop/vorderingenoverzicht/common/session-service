package model

import "gitlab.com/blauwe-knop/connect/go-connect/v2/encoding/base64"

type Session struct {
	Token        string              `json:"token"`
	AppPublicKey string              `json:"appPublicKey"`
	AesKey       base64.Base64String `json:"aesKey"`
	Scope        string              `json:"scope,omitempty"`
	Bsn          string              `json:"bsn,omitempty"`
	CreatedAt    JSONTime            `json:"createdAt"`
	ExpiresAt    JSONTime            `json:"expiresAt"`
}
