package model

type Nonce struct {
	Nonce                        string   `json:"nonce"`
	EphemeralOrganizationKeyPair string   `json:"ephemeralOrganizationKeyPair"`
	CreatedAt                    JSONTime `json:"createdAt"`
}
