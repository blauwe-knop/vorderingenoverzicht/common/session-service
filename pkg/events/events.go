package events

var (
	SES_1 = NewEvent(
		"ses_1",
		"started listening",
		Low,
	)
	SES_2 = NewEvent(
		"ses_2",
		"server closed",
		High,
	)
	SES_3 = NewEvent(
		"ses_3",
		"error creating session db",
		VeryHigh,
	)
	SES_4 = NewEvent(
		"ses_4",
		"error creating session db",
		VeryHigh,
	)
	SES_5 = NewEvent(
		"ses_5",
		"received request for openapi spec as JSON",
		Low,
	)
	SES_6 = NewEvent(
		"ses_6",
		"sent response with openapi spec as JSON",
		Low,
	)
	SES_7 = NewEvent(
		"ses_7",
		"received request for openapi spec as YAML",
		Low,
	)
	SES_8 = NewEvent(
		"ses_8",
		"sent response with openapi spec as YAML",
		Low,
	)
	SES_9 = NewEvent(
		"ses_9",
		"failed to read openapi.json file",
		High,
	)
	SES_10 = NewEvent(
		"ses_10",
		"failed to write fileBytes to response",
		High,
	)
	SES_11 = NewEvent(
		"ses_11",
		"failed to read openapi.yaml file",
		High,
	)
	SES_12 = NewEvent(
		"ses_12",
		"received request for nonce list",
		Low,
	)
	SES_13 = NewEvent(
		"ses_13",
		"sent response with nonce list",
		Low,
	)
	SES_14 = NewEvent(
		"ses_14",
		"received request to create nonce",
		Low,
	)
	SES_15 = NewEvent(
		"ses_15",
		"sent response with created nonce",
		Low,
	)
	SES_16 = NewEvent(
		"ses_16",
		"received request to fetch nonce",
		Low,
	)
	SES_17 = NewEvent(
		"ses_17",
		"sent response with nonce",
		Low,
	)
	SES_18 = NewEvent(
		"ses_18",
		"received request to update nonce",
		Low,
	)
	SES_19 = NewEvent(
		"ses_19",
		"sent response with updated nonce",
		Low,
	)
	SES_20 = NewEvent(
		"ses_20",
		"received request to delete nonce",
		Low,
	)
	SES_21 = NewEvent(
		"ses_21",
		"sent response with deleted nonce",
		Low,
	)
	SES_22 = NewEvent(
		"ses_22",
		"failed to encode response payload",
		High,
	)
	SES_23 = NewEvent(
		"ses_23",
		"failed to decode request payload",
		High,
	)
	SES_24 = NewEvent(
		"ses_24",
		"failed to fetch nonce",
		High,
	)
	SES_25 = NewEvent(
		"ses_25",
		"nonce not found",
		High,
	)
	SES_26 = NewEvent(
		"ses_26",
		"failed to get nonce setting",
		High,
	)
	SES_27 = NewEvent(
		"ses_27",
		"failed to update nonce",
		High,
	)
	SES_28 = NewEvent(
		"ses_28",
		"failed to delete nonce",
		High,
	)
	SES_29 = NewEvent(
		"ses_29",
		"received request for session list",
		Low,
	)
	SES_30 = NewEvent(
		"ses_30",
		"sent response with session list",
		Low,
	)
	SES_31 = NewEvent(
		"ses_31",
		"received request to create session",
		Low,
	)
	SES_32 = NewEvent(
		"ses_32",
		"sent response with created session",
		Low,
	)
	SES_33 = NewEvent(
		"ses_33",
		"received request to fetch session",
		Low,
	)
	SES_34 = NewEvent(
		"ses_34",
		"sent response with session",
		Low,
	)
	SES_35 = NewEvent(
		"ses_35",
		"received request to update session",
		Low,
	)
	SES_36 = NewEvent(
		"ses_36",
		"sent response with updated session",
		Low,
	)
	SES_37 = NewEvent(
		"ses_37",
		"received request to delete session",
		Low,
	)
	SES_38 = NewEvent(
		"ses_38",
		"sent response with deleted session",
		Low,
	)
	SES_39 = NewEvent(
		"ses_39",
		"failed to retrieve session",
		High,
	)
	SES_40 = NewEvent(
		"ses_40",
		"session not found",
		High,
	)
	SES_41 = NewEvent(
		"ses_41",
		"failed to fetch session",
		High,
	)
	SES_42 = NewEvent(
		"ses_42",
		"failed to update session",
		High,
	)
	SES_43 = NewEvent(
		"ses_43",
		"failed to delete session",
		High,
	)
)
