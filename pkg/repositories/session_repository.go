// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

type SessionRepository interface {
	ListSession() (*[]model.Session, error)
	CreateSession(Session model.Session) (*model.Session, error)
	GetSession(id string) (*model.Session, error)
	UpdateSession(id string, Session model.Session) (*model.Session, error)
	DeleteSession(id string) (*model.Session, error)
	CreateNonce(model.Nonce) (*model.Nonce, error)
	GetNonce(string) (*model.Nonce, error)
	DeleteNonce(string) (*model.Nonce, error)
	healthcheck.Checker
}
