// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

package repositories

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"gitlab.com/blauwe-knop/common/health-checker/pkg/healthcheck"

	"gitlab.com/blauwe-knop/vorderingenoverzicht/session-service/pkg/model"
)

type SessionClient struct {
	baseURL string
	apiKey  string
}

var ErrNonceNotFound = errors.New("nonce does not exist")
var ErrSessionNotFound = errors.New("session does not exist")

func NewSessionClient(baseURL string, apiKey string) *SessionClient {
	return &SessionClient{
		baseURL: baseURL,
		apiKey:  apiKey,
	}
}

func (s *SessionClient) ListSession() (*[]model.Session, error) {
	url := fmt.Sprintf("%s/sessions", s.baseURL)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get sessions request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get session: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving session list: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	sessions := []model.Session{}
	err = json.Unmarshal(body, &sessions)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &sessions, nil
}

func (s *SessionClient) CreateSession(session model.Session) (*model.Session, error) {
	url := fmt.Sprintf("%s/sessions", s.baseURL)

	requestBodyAsJson, err := json.Marshal(session)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create post session request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post session: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while creating session: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnSession := model.Session{}
	err = json.Unmarshal(body, &returnSession)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnSession, nil
}

func (s *SessionClient) GetSession(id string) (*model.Session, error) {
	url := fmt.Sprintf("%s/sessions/%s", s.baseURL, id)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get session request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get session: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrSessionNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving session: %d, id: %s", resp.StatusCode, id)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	session := model.Session{}
	err = json.Unmarshal(body, &session)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, id: %s: %v", id, err)
	}

	return &session, nil
}

func (s *SessionClient) UpdateSession(id string, session model.Session) (*model.Session, error) {
	url := fmt.Sprintf("%s/sessions/%s", s.baseURL, id)

	requestBodyAsJson, err := json.Marshal(session)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create update session request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to update session: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while updating session: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnSession := model.Session{}
	err = json.Unmarshal(body, &returnSession)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnSession, nil
}

func (s *SessionClient) DeleteSession(id string) (*model.Session, error) {
	url := fmt.Sprintf("%s/sessions/%s", s.baseURL, id)

	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create delete sessions request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to delete session: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while deleting session: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	session := model.Session{}
	err = json.Unmarshal(body, &session)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &session, nil
}

func (s *SessionClient) CreateNonce(nonce model.Nonce) (*model.Nonce, error) {
	url := fmt.Sprintf("%s/nonces", s.baseURL)

	requestBodyAsJson, err := json.Marshal(nonce)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall request body: %v", err)
	}

	request, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(requestBodyAsJson))
	if err != nil {
		return nil, fmt.Errorf("failed to create post nonce request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to post nonce: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving nonce: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	returnNonce := model.Nonce{}
	err = json.Unmarshal(body, &returnNonce)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &returnNonce, nil
}

func (s *SessionClient) GetNonce(id string) (*model.Nonce, error) {
	url := fmt.Sprintf("%s/nonces/%s", s.baseURL, id)
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create get nonce request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to get nonce: %v", err)
	}

	if resp.StatusCode == http.StatusNotFound {
		return nil, ErrNonceNotFound
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving nonces: %d, id: %s", resp.StatusCode, id)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	nonce := model.Nonce{}
	err = json.Unmarshal(body, &nonce)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json, id: %s: %v", id, err)
	}

	return &nonce, nil
}

func (s *SessionClient) DeleteNonce(id string) (*model.Nonce, error) {
	url := fmt.Sprintf("%s/nonces/%s", s.baseURL, id)

	request, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create delete nonce request: %v", err)
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	client := &http.Client{Timeout: 20 * time.Second}
	resp, err := client.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to delete nonce: %v", err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status while retrieving nonces: %d", resp.StatusCode)
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body: %v", err)
	}

	nonce := model.Nonce{}
	err = json.Unmarshal(body, &nonce)
	if err != nil {
		return nil, fmt.Errorf("failed to decode json: %v", err)
	}

	return &nonce, nil
}

func (s *SessionClient) GetHealthCheck() healthcheck.Result {
	name := "session-service"
	url := fmt.Sprintf("%s/health/check", s.baseURL)
	timeout := 10 * time.Second
	start := time.Now()

	client := http.Client{
		Timeout: timeout,
	}

	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Printf("failed to create health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Authentication", s.apiKey)

	response, err := client.Do(request)
	if err != nil {
		log.Printf("failed to get health check request: %v", err)
		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	body, err := io.ReadAll(response.Body)

	if response.StatusCode != http.StatusOK && response.StatusCode != http.StatusServiceUnavailable {
		if err != nil {
			log.Printf("failed to parse body health check request: %s", err.Error())
		} else {
			log.Printf("failed to parse body health check request: %d - %s", response.StatusCode, string(body))
		}

		return healthcheck.Result{
			Name:         name,
			Status:       healthcheck.StatusError,
			ResponseTime: time.Since(start).Seconds(),
			HealthChecks: []healthcheck.Result{},
		}
	}

	var healthCheckResponse healthcheck.Result

	err = json.Unmarshal(body, &healthCheckResponse)
	if err != nil {
		log.Printf("Unmarshal error: %v", err)
		healthCheckResponse.Status = healthcheck.StatusError
	}

	return healthCheckResponse
}
